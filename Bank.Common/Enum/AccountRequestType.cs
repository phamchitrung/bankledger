﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Common.Enum
{
    public enum AccountRequestType
    {
        Deposit = 1,
        Withdrawal = 2
    }
}
