﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Common.Enum
{
    public enum AccountRequestStatus
    {
        Pending = 1,
        Success = 2,
        Failed = 3,
        Canceled = 4
    }
}
