﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bank.Core.Entity;
using Bank.Service.DTO;

namespace Bank.Service.MappingProfile
{
    public class RequestMapping : Profile
    {
        public RequestMapping()
        {
            CreateMap<RequestDto, Request>().ReverseMap();
        }
    }
}
