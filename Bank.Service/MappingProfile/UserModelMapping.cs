﻿using AutoMapper;
using Bank.Core.Entity;
using Bank.Service.DTO;

namespace Bank.Service.MappingProfile
{
    public class UserModelMapping : Profile
    {
        public UserModelMapping()
        {
            CreateMap<UserDetailDto, User>().ReverseMap();

            CreateMap<AccountInfoDto, User>().ReverseMap();
        }
    }
}
