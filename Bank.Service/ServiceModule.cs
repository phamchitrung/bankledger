﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Bank.Core;
using Bank.Core.Contract;
using Bank.Core.Implementation;

namespace Bank.Service
{
    public class ServiceModule : Module
    {
        private string connectionString;
        public ServiceModule(string connString)
        {
            connectionString = connString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            builder.RegisterModule(new DataModule(connectionString));
            base.Load(builder);
        }
    }
}
