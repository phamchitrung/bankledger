﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bank.Common.Static_Method;
using Bank.Core.Contract;
using Bank.Core.Entity;
using Bank.Service.Contract;
using Bank.Service.Custom_Exception;
using Bank.Service.DTO;

namespace Bank.Service.Service
{
    public class MyAuthenticationService : IMyAuthenticationService
    {
        protected IUnitOfWork unitOfWork;
        public MyAuthenticationService(IUnitOfWork uow)
        {
            this.unitOfWork = uow;
        }

        public void Login(UserDetailDto account)
        {
            var validations = new ValidationGroupException();
            var user = unitOfWork.Get<User>().Query(q => q.Username == account.Username).FirstOrDefault();
            if (user == null || StaticMethod.EncodePass(account.Password) != user.Password)
            {
                validations.Add(new ValidationException("Password", "Your username or password is incorrect"));
            }
            if (validations.ValidationExceptions.Any())
            {
                throw validations;
            }
        }

        public void SaveUser(UserDetailDto accountDetail)
        {
            var newUser = Mapper.Map<User>(accountDetail);
            newUser.Password = accountDetail.Id > 0
                ? unitOfWork.Get<User>().Query(q => q.Id == accountDetail.Id).Select(q => q.Password).FirstOrDefault()
                : StaticMethod.EncodePass(accountDetail.Password);
            unitOfWork.Get<User>().Save(newUser);
        }

        public int GetUserId(string username)
        {
            return unitOfWork.Get<User>().Query(q => q.Username == username).Select(q => q.Id).FirstOrDefault();
        }

        public void ValidateNewAccount(UserDetailDto account)
        {
            var validations = new ValidationGroupException();
            var existUser = unitOfWork.Get<User>().Query(q => q.Username == account.Username);
            if (existUser.Any())
            {
                validations.Add(new ValidationException("Username", "This username is exist!"));
            }
            if (validations.ValidationExceptions.Any())
            {
                throw validations;
            }
        }
    }
}
