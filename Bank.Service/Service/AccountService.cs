﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bank.Common.Enum;
using Bank.Core.Contract;
using Bank.Core.Entity;
using Bank.Service.Contract;
using Bank.Service.Custom_Exception;
using Bank.Service.DTO;

namespace Bank.Service.Service
{
    public class AccountService : IAccountService
    {
        protected IUnitOfWork unitOfWork;
        public AccountService(IUnitOfWork uow)
        {
            this.unitOfWork = uow;
        }

        private void ValidateRequest(RequestDto request)
        {
            var user = unitOfWork.Get<User>().GetEntityById(request.CreateBy);
            var validations = new ValidationGroupException();
            switch (request.RequestType)
            {
                case AccountRequestType.Deposit:
                    break;
                case AccountRequestType.Withdrawal:
                    if (user.AccountBalance < request.Amount)
                    {
                        validations.Add(new ValidationException("Amount", "Your balance is not enough"));
                    }
                    break;
            }
            if (validations.ValidationExceptions.Any())
            {
                throw validations;
            }
        }

        private void ExcuteRequest(RequestDto request)
        {
            var user = unitOfWork.Get<User>().GetEntityById(request.CreateBy);
            switch (request.RequestType)
            {
                case AccountRequestType.Deposit:
                    user.AccountBalance += request.Amount;
                    break;
                case AccountRequestType.Withdrawal:
                    user.AccountBalance -= request.Amount;
                    break;
            }
            unitOfWork.Get<User>().Save(user);
        }
        public void CreateRequest(RequestDto request)
        {
            ValidateRequest(request);
            unitOfWork.Get<Request>().Save(Mapper.Map<Request>(request));
            ExcuteRequest(request);
            unitOfWork.SaveContext();
        }

        public List<RequestDto> GetTransactionHistory(string username)
        {
            var user = unitOfWork.Get<User>().Query(x => x.Username == username).FirstOrDefault();
            return unitOfWork.Get<Request>().Query(x => x.CreateBy == user.Id).OrderByDescending(o => o.CreateDate)
                .Select(s => new RequestDto()
                {
                    Amount = s.RequestType == (int)AccountRequestType.Withdrawal ? -s.Amount : s.Amount,
                    RequestType = (AccountRequestType)s.RequestType,
                    CreateDate = s.CreateDate
                }).ToList();
        }

        public AccountInfoDto GetAccountInfo(string username)
        {
            return unitOfWork.Get<User>().Query(x => x.Username == username).Select(Mapper.Map<AccountInfoDto>)
                .FirstOrDefault();
        }
    }
}
