﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Service.Custom_Exception
{
    public class ValidationException : Exception
    {
        public string Key { get; set; }

        public ValidationException(string key, string message) : base(message)
        {
            this.Key = key;
        }
    }
}
