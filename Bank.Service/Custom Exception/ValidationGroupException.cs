﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Service.Custom_Exception
{
    public class ValidationGroupException : Exception
    {
        public IList<ValidationException> ValidationExceptions { get; set; }

        public ValidationGroupException()
        {
            this.ValidationExceptions = new List<ValidationException>();
        }

        public void Add(ValidationException exception)
        {
            if (exception == null) return;
            this.ValidationExceptions.Add(exception);
        }
    }
}
