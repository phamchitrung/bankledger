﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Service.DTO
{
    public abstract class BaseDto
    {
        public int Id { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
