﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Common.Enum;

namespace Bank.Service.DTO
{
    public class RequestDto : BaseDto
    {
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public AccountRequestType RequestType { get; set; }
        public decimal Amount { get; set; }
        public AccountRequestStatus RequestStatus { get; set; }
    }
}
