﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Service.DTO;

namespace Bank.Service.Contract
{
    public interface IMyAuthenticationService
    {
        void Login(UserDetailDto account);
        void SaveUser(UserDetailDto accountDetail);
        int GetUserId(string username);
        void ValidateNewAccount(UserDetailDto acccount);
    }
}
