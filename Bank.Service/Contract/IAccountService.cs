﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Service.DTO;

namespace Bank.Service.Contract
{
    public interface IAccountService
    {
        void CreateRequest(RequestDto request);
        List<RequestDto> GetTransactionHistory(string username);
        AccountInfoDto GetAccountInfo(string username);
    }
}
