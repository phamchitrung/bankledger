﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bank.Common.Enum;

namespace Bank.Web.Models
{
    public class RequestViewModel : BaseViewModel
    {
        public AccountRequestType RequestType { get; set; }
        public AccountRequestStatus RequestStatus { get; set; }
        [Display(Description = "Amount of money")]
        public decimal Amount { get; set; }
    }
}