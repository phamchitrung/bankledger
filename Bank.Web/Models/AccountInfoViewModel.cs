﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Service.DTO;

namespace Bank.Web.Models
{
    public class AccountInfoViewModel
    {
        public AccountInfoDto AccountInfo { get; set; } 
        public List<RequestDto> History { get; set; }
    }
}