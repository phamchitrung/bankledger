﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Bank.Web.Models
{
    public class ResultViewModel : JsonResult
    {
        public int ResultType { get; set; }
        public Dictionary<string, string> ValidationMessages { get; set; }
        public View HtmlContent { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }

        public ResultViewModel()
        {

        }

        public override void ExecuteResult(ControllerContext context)
        {
            this.Data = new
            {
                ResultType = this.ResultType,
                ValidationMessages = this.ValidationMessages,
                HtmlContent = this.HtmlContent,
                Message = this.Message,
                RedirectUrl = this.RedirectUrl
            };
            base.ExecuteResult(context);
        }
    }
}