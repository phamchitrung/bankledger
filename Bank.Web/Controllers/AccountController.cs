﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Bank.Common.Enum;
using Bank.Service.Contract;
using Bank.Service.DTO;
using Bank.Web.Filter;
using Bank.Web.Models;

namespace Bank.Web.Controllers
{
    [LoggedInPermissionFilter]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;
        private readonly IMyAuthenticationService _myAuthenticationService;

        public AccountController(IAccountService accountService, IMyAuthenticationService myAuthenticationService)
        {
            _accountService = accountService;
            _myAuthenticationService = myAuthenticationService;
        }
        
        // GET: Account
        public ActionResult Index()
        {
            var username = Session["Username"].ToString();
            var model = new AccountInfoViewModel
            {
                AccountInfo = _accountService.GetAccountInfo(username),
                History = _accountService.GetTransactionHistory(username)
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateRequest(int requestType)
        {
            var model = new RequestViewModel()
            {
                RequestType = (AccountRequestType)requestType,
                RequestStatus = AccountRequestStatus.Pending,
                Amount = 0
            };
            return View("CreateRequest", model);
        }

        [HttpPost]
        [ActionName("CreateRequest")]
        public ActionResult CreateRequest(RequestViewModel request)
        {
            if (!ModelState.IsValid) return View("CreateRequest", request);
            var dto = Mapper.Map<RequestDto>(request);
            dto.CreateDate = DateTime.Now;
            dto.CreateBy = _myAuthenticationService.GetUserId(Session["Username"].ToString());
            _accountService.CreateRequest(dto);
            return RedirectToAction("Index");
        }
    }
}