﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using AutoMapper;
using Bank.Service.Contract;
using Bank.Service.DTO;
using Bank.Web.Filter;
using Bank.Web.Models;

namespace Bank.Web.Controllers
{
    
    public class HomeController : BaseController
    {
        private IMyAuthenticationService myAuthenticationService;

        public HomeController()
        {
            this.myAuthenticationService = AutofacDependencyResolver.Current.GetService<IMyAuthenticationService>();
        }

        [HomePermissionFilter]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HomePermissionFilter]
        [HttpPost]
        [ActionName("Login")]
        public ActionResult Login(LoginViewModel userDetail)
        {
            if (!ModelState.IsValid) return View("Login", userDetail);
            myAuthenticationService.Login(Mapper.Map<UserDetailDto>(userDetail));
            Session["Username"] = userDetail.Username;
            return RedirectToAction("Index", "Account");
        }

        [LoggedInPermissionFilter]
        public ActionResult Logout()
        {
            Session["Username"] = null;
            return RedirectToAction("Login");
        }

        [HomePermissionFilter]
        [HttpGet]
        public ActionResult Register()
        {
            var viewModel = new RegisterViewModel();
            return View(viewModel);
        }

        [HomePermissionFilter]
        [HttpPost]
        [ActionName("Register")]
        public ActionResult Register(RegisterViewModel userDetail)
        {
            if (!ModelState.IsValid) return View("Register", userDetail);
            var userModel = Mapper.Map<UserDetailDto>(userDetail);
            myAuthenticationService.ValidateNewAccount(userModel);
            myAuthenticationService.SaveUser(userModel);
            TempData["Message"] = "Register successfully!";
            return RedirectToAction("Index", "Account");
        }
    }
}