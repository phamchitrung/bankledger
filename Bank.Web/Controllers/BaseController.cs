﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bank.Service.Custom_Exception;
using Bank.Web.Models;

namespace Bank.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            if (exception.GetType() == typeof(ValidationGroupException))
            {
                var validationGroup = (exception as ValidationGroupException);
                if (validationGroup != null && validationGroup.ValidationExceptions.Any())
                {
                    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        var validationMessages = new Dictionary<string, string>();
                        foreach (var validationException in validationGroup.ValidationExceptions)
                        {
                            if (validationException == null) continue;
                            validationMessages.Add(validationException.Key,
                                validationException.Message);
                        }
                        filterContext.Result = new ResultViewModel()
                        {
                            ValidationMessages = validationMessages
                        };
                    }
                    else
                    {
                        foreach (var validationException in validationGroup.ValidationExceptions)
                        {
                            if (validationException == null) continue;
                            filterContext.Controller.ViewData.ModelState.AddModelError(validationException.Key,
                                validationException.Message);
                        }
                        var action = filterContext.RequestContext.RouteData.Values["action"].ToString();
                        filterContext.Result = View(action, filterContext.Controller.TempData["model"]);

                    }
                    filterContext.ExceptionHandled = true;
                }
            }
            base.OnException(filterContext);
        }
    }
}