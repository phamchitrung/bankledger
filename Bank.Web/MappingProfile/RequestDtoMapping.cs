﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Bank.Service.DTO;
using Bank.Web.Models;

namespace Bank.Web.MappingProfile
{
    public class RequestDtoMapping: Profile
    {
        public RequestDtoMapping()
        {
            CreateMap<RequestDto, RequestViewModel>().ReverseMap();
        }
    }
}