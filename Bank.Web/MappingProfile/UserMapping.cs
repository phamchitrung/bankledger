﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Bank.Service.DTO;
using Bank.Web.Models;

namespace Bank.Web.MappingProfile
{
    public class UserMapping : Profile
    {
        public UserMapping()
        {
            CreateMap<UserDetailDto, RegisterViewModel>().ReverseMap();

            CreateMap<UserDetailDto, LoginViewModel>().ReverseMap();
        }
    }
}