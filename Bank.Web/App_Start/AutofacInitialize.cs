﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Bank.Service;
using Bank.Service.Contract;
using Bank.Service.MappingProfile;
using Bank.Service.Service;
using Bank.Web.MappingProfile;

namespace Bank.Web
{
    public class AutofacInitialize
    {
        public static void InitializeIoc()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            var connectionString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            RegisterService(builder);
            builder.RegisterModule(new ServiceModule(connectionString));

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            InitMapper();
        }

        public static void RegisterService(ContainerBuilder builder)
        {
            builder.RegisterType<MyAuthenticationService>().As<IMyAuthenticationService>().InstancePerRequest();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerRequest();
        }

        public static void InitMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new UserMapping());
                cfg.AddProfile(new UserModelMapping());
                cfg.AddProfile(new RequestMapping());
                cfg.AddProfile(new RequestDtoMapping());
            });
        }
    }
}