﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Entity;

namespace Bank.Core.Contract
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetEntityById(int id);
        IList<TEntity> GetEntitiesPaging(PagingQuery<TEntity> query);
        TEntity Save(TEntity entity);
        void SaveAll(IList<TEntity> entities);
        bool Delete(int id);
        IList<TEntity> List();

        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> where, PagingQuery<TEntity> pageQuery = null);
    }
}
