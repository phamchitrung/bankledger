﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Entity;

namespace Bank.Core.Contract
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveContext();
        IRepository<T> Get<T>() where T : BaseEntity;
    }
}
