﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Contract;
using Bank.Core.Entity;
using Bank.Core.EntityMap;

namespace Bank.Core.Implementation
{
    public class DefaultContext : DbContext, IDbContext
    {
        public DefaultContext(string connectionString) : base(connectionString)
        {

        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public new DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            return base.Entry(entity);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RequestMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
