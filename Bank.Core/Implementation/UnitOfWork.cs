﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Contract;
using Bank.Core.Entity;

namespace Bank.Core.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext _dbContext;
        private readonly Lazy<Dictionary<Type, object>> _repos = null;
        public UnitOfWork(IDbContext dbContext)
        {
            this._dbContext = dbContext;
            _repos = new Lazy<Dictionary<Type, object>>(() => new Dictionary<Type, object>());
        }

        public void SaveContext()
        {
            _dbContext.SaveChanges();
        }

        public IRepository<T> Get<T>() where T : BaseEntity
        {
            object value;
            if (_repos.Value.TryGetValue(typeof(T), out value))
            {
                return value as IRepository<T>;
            }
            var newRepo = new BaseRepository<T>(_dbContext);
            _repos.Value.Add(typeof(T), newRepo);
            return newRepo;
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
