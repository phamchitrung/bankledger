﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Contract;
using Bank.Core.Entity;

namespace Bank.Core.Implementation
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext dbContext;

        public BaseRepository(IDbContext context)
        {
            dbContext = context;
        }

        public TEntity GetEntityById(int id)
        {
            return dbContext.Set<TEntity>().Find(id);
        }

        public IList<TEntity> GetEntitiesPaging(PagingQuery<TEntity> query)
        {
            var dbEntities = dbContext.Set<TEntity>();
            var orderedEntities = query.SortOrder == SortOrder.Ascending
                ? dbEntities.OrderBy(x => x.Id)
                : dbEntities.OrderByDescending(x => x.Id);
            return orderedEntities.Skip(query.PageIndex * query.PageLength).Take(query.PageLength).ToList();
        }

        public TEntity Save(TEntity entity)
        {
            if (entity == null) return null;
            var dbEntity = dbContext.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
            if (dbEntity == null)
            {
                dbContext.Set<TEntity>().Add(entity);
            }
            else
            {
                dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
            }
            dbContext.SaveChanges();
            return dbContext.Entry(entity).Entity;
        }

        public bool Delete(int id)
        {
            var entity = GetEntityById(id);
            if (entity == null) return false;
            dbContext.Entry(entity).State = EntityState.Deleted;
            return true;
        }

        public IList<TEntity> List()
        {
            return dbContext.Set<TEntity>().ToList();
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> where, PagingQuery<TEntity> pageQuery = null)
        {
            var dbEntities = dbContext.Set<TEntity>().Where(@where);
            if (pageQuery == null) return dbEntities;
            var orderedEntities = pageQuery.SortOrder == SortOrder.Ascending
                ? dbEntities.OrderBy(x => x.Id)
                : dbEntities.OrderByDescending(x => x.Id);
            return orderedEntities.Skip(pageQuery.PageIndex * pageQuery.PageLength).Take(pageQuery.PageLength);
        }
        public void SaveAll(IList<TEntity> entities)
        {
            if (entities == null) return;
            foreach (var entity in entities)
            {
                var dbEntity = dbContext.Set<TEntity>().FirstOrDefault(x => x.Id == entity.Id);
                if (dbEntity == null)
                {
                    dbContext.Set<TEntity>().Add(entity);
                }
                else
                {
                    dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
                }
            }
        }
    }
}
