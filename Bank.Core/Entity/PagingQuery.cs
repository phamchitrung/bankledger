﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.Entity
{
    public class PagingQuery<T> where T : BaseEntity
    {
        public int PageIndex { get; set; }
        public int PageLength { get; set; }
        public SortOrder SortOrder { get; set; }
        public int OrderColumn { get; set; }
        public string SearchText { get; set; }

        public PagingQuery(int pageIndex = 0, int pageLength = 1, SortOrder order = SortOrder.Ascending, int orderColumn = 1, string searchText = null)
        {
            PageIndex = pageIndex;
            PageLength = pageLength;
            SortOrder = order;
            OrderColumn = orderColumn;
            SearchText = searchText;
        }
    }
}
