﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.Entity
{
    public class Request : BaseEntity
    {
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int RequestType { get; set; }
        public decimal Amount { get; set; }
        public int RequestStatus { get; set; }
    }
}
