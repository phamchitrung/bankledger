﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.Entity
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
