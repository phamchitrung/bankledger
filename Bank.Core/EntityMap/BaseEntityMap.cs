﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Entity;

namespace Bank.Core.EntityMap
{
    public abstract class BaseEntityMap<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : BaseEntity
    {
        protected BaseEntityMap()
        {
            this.HasKey<int>(s => s.Id);
            this.Property(x => x.Timestamp).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed).IsRowVersion();
        }
    }
}
