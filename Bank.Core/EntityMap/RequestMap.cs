﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Entity;

namespace Bank.Core.EntityMap
{
    public class RequestMap : BaseEntityMap<Request>
    {
        public RequestMap()
        {
            this.ToTable("Request");
            this.Property(x => x.Id)
                .HasColumnName("RequestId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Amount).IsRequired();
            this.Property(x => x.CreateBy).IsRequired();
            this.Property(x => x.CreateDate).IsRequired();
            this.Property(x => x.RequestType).IsRequired();
        }
    }
}
