﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Core.Entity;

namespace Bank.Core.EntityMap
{
    public class UserMap : BaseEntityMap<User>
    {
        public UserMap()
        {
            this.ToTable("User");
            this.Property(x => x.Id)
                .HasColumnName("UserId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(x => x.Fullname).IsRequired();
            this.Property(x => x.Password).IsRequired();
            this.Property(x => x.Username).IsRequired();
            this.Property(x => x.AccountBalance).IsRequired();
        }
    }
}
