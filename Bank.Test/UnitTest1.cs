﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Bank.Common.Enum;
using Bank.Core.Contract;
using Bank.Core.Entity;
using Bank.Service.Contract;
using Bank.Service.Custom_Exception;
using Bank.Service.DTO;
using Bank.Service.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Bank.Test
{
    [TestClass]
    public class AccountTest
    {
        private AccountService _accountService { get; set; }

        [TestInitialize]
        public void Preparation()
        {
            var mockUow = new Mock<IUnitOfWork>();
            mockUow.Setup(s => s.Get<User>().Query(It.IsAny<Expression<Func<User, bool>>>(), null)).Returns(
                new List<User>
                {
                    new User
                    {
                        Id = 1
                    }
                }.AsQueryable);
            mockUow.Setup(s => s.Get<User>().GetEntityById(It.IsAny<int>())).Returns(new User()
            {
                AccountBalance = 2000
            });
            mockUow.Setup(s => s.Get<Request>().Query(It.IsAny<Expression<Func<Request, bool>>>(), null)).Returns(new List<Request>
            {
                new Request()
                {
                    RequestType = (int)AccountRequestType.Deposit,
                    Amount = 3000,
                    CreateBy = 1
                },
                new Request()
                {
                    RequestType = (int)AccountRequestType.Withdrawal,
                    Amount = 1000,
                    CreateBy = 1
                }
            }.AsQueryable());
            _accountService = new AccountService(mockUow.Object);
        }


        [TestMethod]
        [ExpectedException(typeof(ValidationGroupException))]
        public void CreateRequest_Withdraw_5000_Exception()
        {
            _accountService.CreateRequest(new RequestDto()
            {
                RequestType = AccountRequestType.Withdrawal,
                CreateBy = 1,
                Amount = 5000
            });
        }

        [TestMethod]
        public void GetTransactionHistory_2()
        {
            var history = _accountService.GetTransactionHistory("test");
            Assert.AreEqual(history.Count, 2);
        }
    }
}
